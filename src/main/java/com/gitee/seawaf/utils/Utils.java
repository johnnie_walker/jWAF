package com.gitee.seawaf.utils;

import javax.servlet.http.HttpSession;

import com.gitee.seawaf.SessionInfo;
import com.gitee.seawaf.config.WafConfig;

import ognl.Ognl;

/**
 * 工具类
 * @author haison
 *
 */
public class Utils {
	
	/**
	 * 合并url，防止同一个url因参数不同而识别为不同的url
	 * @param url
	 * @return
	 */
	public static String mergeUrl(String url){
		return url.replaceAll("=\\d+", "=#{number}");
	}
	
	/**
	 * 从session中获取用户信息
	 * @param session
	 * @return
	 */
	public static String getUserNameFromSession(HttpSession session){
		try{
			String nameAttr = WafConfig.getInstance().getApplication().getSessionUserAttributeName();
			String namePath = WafConfig.getInstance().getApplication().getSessionUserNamePath();
			String idPath=WafConfig.getInstance().getApplication().getSessionUserIdPath();
			Object name = Ognl.getValue(namePath, session.getAttribute(nameAttr));
			Object id=Ognl.getValue(idPath, session);
			if(name!=null && id!=null){
				return id.toString()+"("+name.toString()+")";
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 从缓存中获取用户信息
	 * @param sessionId
	 * @return
	 */
	public static String getUserInfoFromCache(String sessionId){
		Object sessionInfo = CacheUtils.getInstance().get("sessions", sessionId);
		if(sessionInfo!=null && sessionInfo instanceof SessionInfo){
			SessionInfo info = (SessionInfo)sessionInfo;
			return info.getUserId()+"("+info.getUserName()+")";
		}
		return null;
	}
	
	/**
	 * 封禁指定的IP地址
	 * @param ip
	 * @param reason
	 */
	public static void denyIp(String ip,String reason){
		int times = CacheUtils.getInstance().getIntValue("ip.logs", ip);
		switch(times){
			case -1:
				CacheUtils.getInstance().put("ip5", ip, reason);
				CacheUtils.getInstance().put("ip.logs", ip, 5);
				times=5;
				break;
			case 5:
				CacheUtils.getInstance().put("ip10", ip, reason);
				CacheUtils.getInstance().put("ip.logs", ip, 10);
				times=10;
				break;
			case 10:
				CacheUtils.getInstance().put("ip30", ip, reason);
				CacheUtils.getInstance().put("ip.logs", ip, 30);
				times=30;
				break;
			case 30:
				CacheUtils.getInstance().put("ip99", ip, reason);
				CacheUtils.getInstance().put("ip.logs", ip, 99);
				times=99;
				break;
			default:
				CacheUtils.getInstance().put("ip5", ip, reason);
				CacheUtils.getInstance().put("ip.logs", ip, 5);
				times=5;
		}
	}
}
